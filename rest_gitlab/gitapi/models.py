from django.db import models


# Create your models here.
class ProjectId(models.Model):
    pid = models.IntegerField(primary_key=True)


class Branches(models.Model):
    name = models.CharField(max_length=30)
    protected = models.BooleanField()
    web_url = models.CharField(max_length=200)
    project = models.ForeignKey(ProjectId, on_delete=models.CASCADE, related_name='branch')

    def __str__(self):
        return self.name