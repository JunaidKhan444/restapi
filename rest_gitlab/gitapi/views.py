from cgitb import lookup
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from .models import Branches, ProjectId
from .serializers import BranchesSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
import requests, json




class DeleteUpdate(RetrieveUpdateDestroyAPIView):
    serializer_class = BranchesSerializer
    queryset = Branches.objects.all()
    
    #lookup_field = None
    def get_object(self):
        print("***********",self.kwargs['name'])
        obj = Branches.objects.get(project__pid = self.kwargs['pk'],name = self.kwargs['name'])
        return obj

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class BranchList(ListCreateAPIView):
    serializer_class = BranchesSerializer
    #lookup_field = 'pk'

    def list(self, request, *args, **kwargs):
        pk = kwargs['pk']
        print(pk)
        queryset1 = self.get_queryset()
        serializer = BranchesSerializer(queryset1, many=True)
        return Response(serializer.data)
        #return Response()

    def get_queryset(self):
        pk = self.kwargs['pk']
        print("Query Set ",pk)
        queryset = Branches.objects.filter(project__pid=pk)
        return queryset

    def create(self,request,*args,**kwargs):
        #print(request.POST['name'])
        pk = request.data['project']
        obj,created = ProjectId.objects.get_or_create(pid=pk)
        if created == True:
            obj.save()
        # print(pk,"00000000000000")
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        #print(serializer)
        # print(kwargs['pk'])
        # print(args) 
        
        #return Response()

    
    

