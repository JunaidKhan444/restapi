from django.core.management.base import BaseCommand, CommandError
from gitapi.models import Branches, ProjectId
import requests
import json

class Command(BaseCommand):
    help = 'Fetches Data from Gitlab'

    def add_arguments(self, parser):
        parser.add_argument('pid', nargs='+', type=int)

    def handle(self, *args, **kwargs):
        pk = kwargs['pid'][0]
        branches_url = "https://gitlab.com/api/v4/projects/{}/repository/branches?pagination=keyset&per_page=100".format(pk)
        response_data = requests.get(branches_url)
        
        
        obj1 = ProjectId(pid=pk)
        obj1.save()      
        data = json.loads(response_data.text)
        #tags_data = json.loads(tags_rawdata.text)
        for i in range(len(data)):
            name = data[i]['name']
            protected =  data[i]['protected']
            web_url =  data[i]['web_url']
            #print("***********"+name+"**************")
            obj2,created2 = Branches.objects.get_or_create(project_id = pk,protected =  data[i]['protected'],name = data[i]['name'],web_url=data[i]['web_url'],project = obj1)
            if created2 == True:
                obj2.save()

            """obj = Branches(name = name, protected = protected, web_url = web_url,project = obj1)
            obj.save()"""
        
        #branches = Branches.objects.all()
        
            

        self.stdout.write(self.style.SUCCESS('Successfully Feteched data'))