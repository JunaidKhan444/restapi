from rest_framework import serializers
from .models import Branches

class BranchesSerializer(serializers.ModelSerializer):
    #project_id = serializers.IntegerField()
    class Meta:
        model = Branches
        fields = ['name','protected', 'web_url','project']