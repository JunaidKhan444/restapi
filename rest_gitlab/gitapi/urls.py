from django.urls import path
from .import views

urlpatterns = [
    path('branches/<int:pk>/', views.BranchList.as_view()),
    #path('branch/<int:pk>/', views.snippet_detail),
    path('delete/<int:pk>/<str:name>', views.DeleteUpdate.as_view()),
]